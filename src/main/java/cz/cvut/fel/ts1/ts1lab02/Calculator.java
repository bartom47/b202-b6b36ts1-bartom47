package cz.cvut.fel.ts1.ts1lab02;

import java.util.stream.LongStream;

/**
 *
 * @author Matej
 */
public class Calculator {
    public Long factorial(Integer n){
        return LongStream.rangeClosed(1, n).reduce(1, (long x, long y) -> x * y);
    }
}
