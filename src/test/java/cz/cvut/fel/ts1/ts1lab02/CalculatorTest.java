package cz.cvut.fel.ts1.ts1lab02;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Matej
 */
public class CalculatorTest {
    
    private final Calculator instance;

    public CalculatorTest() {
        this.instance = new Calculator();
    }        

    /**
     * Test of factorial method, of class CalculatorTest.
     */
    @Test
    public void testFactorialEqualsOneForOne() {        
        testFactorialForInputs(1L, 1);
    }
    
    /**
     * Test of factorial method, of class CalculatorTest.
     */
    @Test
    public void testFactorialEqualsHundredTwentyForFive() {
        testFactorialForInputs(120L, 5);
    }
    
    /**
     * Test of factorial method, of class CalculatorTest.
     */
    @Test
    public void testFactorialEqualsOneForZero() {
        testFactorialForInputs(1L, 0);
    }    
    
    protected void testFactorialForInputs(Long expected, Integer argument){
        final Long actual = instance.factorial(argument);
        assertEquals(expected, actual);
    }
    
}
